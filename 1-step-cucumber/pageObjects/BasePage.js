
const EC = protractor.ExpectedConditions;

class BasePage {
  constructor(baseUrl) {
    this.baseUrl = baseUrl || browser.baseUrl;
  }

  /**
   * wait until element is visible and present in page
   * @param elm protractor ElementFinder
   * @returns {promise} return protractor browser waiter
   */
  waitForElement(elm) {
    return browser.wait(EC.presenceOf(elm), 25000)
      .then(() => browser.wait(EC.visibilityOf(elm), 25000));
  }

  /**
   * open browser page
   * @return {promise.Promise<any>}
   */
  open() {
    return browser.get(this.baseUrl);
  }

  /**
   * get browser URL
   * @return {Promise<string>} current url
   */
  getUrl() {
    return browser.getCurrentUrl();
  }
}

module.exports = BasePage;
