exports.config = {
  seleniumAddress: 'http://localhost:4444/wd/hub',
  getPageTimeout: 6000,
  allScriptsTimeout: 25000,
  specs: ['./features/*.feature'],
  baseUrl: 'http://www.way2automation.com/angularjs-protractor/webtables/',
  framework: 'custom',
  frameworkPath: require.resolve('protractor-cucumber-framework'),
  capabilities: {
    browserName: 'chrome',
  },
  cucumberOpts: {
    require: './step_definitions/*.js',
  },
};
