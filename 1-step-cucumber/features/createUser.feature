Feature: Adding new user

  Scenario: User is added to the table successfully
    Given I open home page
    When I can create a user with following data:
      | First Name | Last Name | User Name | Password | Customer    | Role  | E-mail  | Cell Phone |
      | Jon        | Smith     | SJon      | qwerty   | Company AAA | Admin | q@q.com | 33333333   |
    Then I verify user with following data is created:
      | First Name | E-mail  |
      | Jon        | q@q.com |
    When I can create a user with following data:
      | First Name | User Name | Customer    | Role  | E-mail    | Cell Phone |
      | Jonnew     | Jonnew    | Company AAA | Admin | qq@qq.com | 2222       |
    Then I verify user with following data is created:
      | First Name | Cell Phone |
      | Jonnew     | 2222       |
    Then I can delete user with data:
      | First Name | E-mail    |
      | Jon        | q@q.com   |
      | Jonnew     | qq@qq.com |
