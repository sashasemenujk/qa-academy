const EC = protractor.ExpectedConditions;

class BasePage {

  get(url) {
    browser.get(url);
  }

  waitForElement(elm) {
    return browser.wait(EC.presenceOf(elm), 5000)
      .then(() => browser.wait(EC.visibilityOf(elm), 5000))
  }

}

module.exports = BasePage;
