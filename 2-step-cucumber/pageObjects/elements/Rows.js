const BaseElement = require('./BaseElement');
const Row = require('./Row');

class Rows extends BaseElement {
  constructor(){
    super();
    this.listRows = $$('tr[ng-repeat="dataRow in displayedCollection"]');
    this.row = {
      fName: 'td:nth-of-type(1)'
    }
  }

  listOfRows() {
    return this.listRows.map((row,index) => ({
      index: index,
      FirstName : this.row.fName.getText()
      // ......
    }))
  }

  getRowByData(data){
    return this.listOfRows().then(arrRows => arrRows.filter(row =>
      row.FirstName === data.row.FirstName
      && row.LastName === data.row.LastName
    )).then(arrResult => {
      if(arrResult.length === 0) throw Error('Can"t find row');
      return this.getRowByIndex(arrRows[0].index)
    })
  }

  getRowByIndex(index) {
    return new Row(this.listRows.get(index))
  }

  findRowByFName(fName) {
    return this.listRows.filter(row => row.$(this.row.fName).getText().then(text => text === fName))
      .then(rowArr => {
        if (rowArr.length === 0) throw Error(`Row with first name "${fName}" not exist`);
        return new Row(rowArr[0])
      })
  }


}

module.exports = Rows;
